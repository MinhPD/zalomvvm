package com.example.minhpd3.zalomvvm;


import java.util.ArrayList;

/**
 * Created by MinhPD3 on 2/8/2017.
 */

public class UserModel {
    private String name;
    private int icon;
    private int id;
    private int image;
    private String comment;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public UserModel(int id, int icon, String name) {
        this.name = name;
        this.icon = icon;
        this.id = id;
    }

    public UserModel(int id, int icon, String name, int image, String comment) {
        this.id = id;
        this.icon = icon;
        this.name = name;
        this.image = image;
        this.comment = comment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public static ArrayList<UserModel> getFakeData() {
        int iconUser = R.drawable.icon_user_default;
        ArrayList<UserModel> list = new ArrayList<>();
        list.add(new UserModel(1, iconUser, "Minh"));
        list.add(new UserModel(2, iconUser, "Binh"));
        list.add(new UserModel(3, iconUser, "Toa"));
        list.add(new UserModel(4, iconUser, "Sang"));
        list.add(new UserModel(5, iconUser, "Nhu"));
        list.add(new UserModel(6, iconUser, "La"));
        list.add(new UserModel(7, iconUser, "Binh"));
        list.add(new UserModel(8, iconUser, "Minh"));
        return list;
    }

    public static ArrayList<UserModel> getFakeDataSearch() {
        int iconUser = R.drawable.icon_user_default;
        ArrayList<UserModel> list = new ArrayList<>();
        list.add(new UserModel(1, iconUser, "Minh"));
        list.add(new UserModel(2, iconUser, " Binh"));
        return list;
    }

    public static ArrayList<UserModel> getFakeDataForTimeLine() {
        ArrayList<UserModel> list = new ArrayList<>();
        int iconUser = R.drawable.icon_user_default;
        int image1 = R.drawable.image1;
        int image2 = R.drawable.image2;
        int image3 = R.drawable.image3;
        int image4 = R.drawable.image4;
        list.add(new UserModel(1, iconUser, "Minh", image1, "Tam Binh An"));
        list.add(new UserModel(2, iconUser, "Binh", image2, ""));
        list.add(new UserModel(3, iconUser, "Toa", image3, "Tien Day Tui"));
        list.add(new UserModel(4, iconUser, "Sang", image4, "Tinh Day Tim"));

        return list;
    }
}
