package com.example.minhpd3.zalomvvm.MessageFragment;


import com.example.minhpd3.zalomvvm.MainContactListener;

/**
 * Created by MinhPD3 on 2/7/2017.
 */

public class MessageFragmentVM {
    public final MessageFragmentModel model = new MessageFragmentModel();
    private MainContactListener.SearchCreateClickListener mListener;

    public void onSearchClick() {
        mListener.searchClick();
    }

    public void onCreateGroupClick() {
        mListener.createGroupClick();
    }

    public void setListener(MainContactListener.SearchCreateClickListener listener) {
        mListener = listener;
    }
}
