package com.example.minhpd3.zalomvvm.MessageFragment;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.minhpd3.zalomvvm.BaseAdapter;
import com.example.minhpd3.zalomvvm.MainContactListener;
import com.example.minhpd3.zalomvvm.R;
import com.example.minhpd3.zalomvvm.UserModel;
import com.example.minhpd3.zalomvvm.databinding.RecyclerMessageItemBinding;

/**
 * Created by MinhPD3 on 2/8/2017.
 */

public class RecyclerMessageAdapter extends BaseAdapter<UserModel, RecyclerMessageAdapter.ViewHolder> {

    private MainContactListener.ItemUserAdapterClickListener mListener;
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerMessageItemBinding binding;
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_message_item, parent, false);
        Log.e("ViewHolder", "onCreateViewHolder");
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        UserModel model = mItems.get(position);
        holder.bind(model);

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private RecyclerMessageItemBinding binding;
        private RecyclerMessageVM messageVM;


        public ViewHolder(RecyclerMessageItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            messageVM = new RecyclerMessageVM();

        }

        public void bind(UserModel model) {
            binding.setViewModel(messageVM);
           // binding.executePendingBindings();
            messageVM.setNameUser(model.getName());
            messageVM.setIconUser(model.getIcon());
            messageVM.setIdUser(model.getId());
            messageVM.setListener(mListener);
        }
    }
    public void setListener(MainContactListener.ItemUserAdapterClickListener listener){
        mListener = listener;
    }
}
