package com.example.minhpd3.zalomvvm.TimeLineFragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.minhpd3.zalomvvm.R;
import com.example.minhpd3.zalomvvm.databinding.FragmentTimelineLayoutBinding;

/**
 * Created by MinhPD3 on 2/9/2017.
 */

public class TimeLineFragment extends Fragment {

    private TimeLineFragmentVM mFragmentVM;
    private FragmentTimelineLayoutBinding mBinding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_timeline_layout, container, false);
        mFragmentVM = new TimeLineFragmentVM();
        mBinding.setViewModel(mFragmentVM);
        return mBinding.getRoot();
    }
}
