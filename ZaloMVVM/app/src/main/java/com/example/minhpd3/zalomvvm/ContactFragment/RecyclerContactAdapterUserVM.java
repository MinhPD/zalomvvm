package com.example.minhpd3.zalomvvm.ContactFragment;

import android.util.Log;

import com.example.minhpd3.zalomvvm.MainContactListener;

/**
 * Created by MinhPD3 on 2/10/2017.
 */

public class RecyclerContactAdapterUserVM {
    public RecyclerContactAdapterUserModel model = new RecyclerContactAdapterUserModel();
    private MainContactListener.ItemUserAdapterClickListener mItemClickListener;

    public void setIcon(int icon) {
        model.setIcon(icon);
    }

    public void setName(String name) {
        model.setName(name);
    }

    public void setId(int id) {
        model.setId(id);
    }

    public void onItemClick(int id) {
        Log.e("UserVM", "" + id);
        mItemClickListener.itemClick(id);
    }

    public void setListener(MainContactListener.ItemUserAdapterClickListener listener) {
        mItemClickListener = listener;
    }
}
