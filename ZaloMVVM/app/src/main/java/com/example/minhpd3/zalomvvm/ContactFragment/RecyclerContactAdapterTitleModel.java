package com.example.minhpd3.zalomvvm.ContactFragment;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.example.minhpd3.zalomvvm.BR;

/**
 * Created by MinhPD3 on 2/10/2017.
 */

public class RecyclerContactAdapterTitleModel extends BaseObservable {
    private String title;

    @Bindable
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        notifyPropertyChanged(BR.title);
    }
}
