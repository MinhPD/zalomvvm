package com.example.minhpd3.zalomvvm.SearchFragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.minhpd3.zalomvvm.R;
import com.example.minhpd3.zalomvvm.UserModel;
import com.example.minhpd3.zalomvvm.databinding.FragmentSearchLayoutBinding;

import java.util.ArrayList;


/**
 * Created by MinhPD3 on 2/9/2017.
 */

public class SearchFragment extends Fragment {

    private SearchFragmentVM mSearchVM;
    private FragmentSearchLayoutBinding mBinding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mSearchVM = new SearchFragmentVM();
        mBinding = FragmentSearchLayoutBinding.inflate(inflater, container, false);
        mBinding.setViewModel(mSearchVM);

        setHasOptionsMenu(true);

        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerSearchAdapter adapter = new RecyclerSearchAdapter();
        ArrayList<UserModel> list = UserModel.getFakeDataSearch();
        adapter.updateItems(list);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        RecyclerView recyclerView = mBinding.recyclerSearch;
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        RecyclerSearchAdapter adapter1 = new RecyclerSearchAdapter();
        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        RecyclerView recyclerSuggest = mBinding.recyclerSuggest;
        adapter1.updateItems(list);
        recyclerSuggest.setLayoutManager(layoutManager1);
        recyclerSuggest.setAdapter(adapter1);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.message_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setIconified(false);
        searchView.setQueryHint("Tim kiem");

        super.onCreateOptionsMenu(menu, inflater);
    }
}
