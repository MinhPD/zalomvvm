package com.example.minhpd3.zalomvvm.ContactFragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.minhpd3.zalomvvm.MainContactListener;
import com.example.minhpd3.zalomvvm.R;
import com.example.minhpd3.zalomvvm.databinding.FragmentContactLayoutBinding;

import java.util.ArrayList;

/**
 * Created by MinhPD3 on 2/9/2017.
 */

public class ContactFragment extends Fragment {
    private ContactFragmentVM mContactVM;
    private FragmentContactLayoutBinding mBinding;
    private MainContactListener.SearchCreateClickListener mSearchCreateClickListener;
    private MainContactListener.ItemUserAdapterClickListener mItemClickListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_contact_layout, container, false);
        mContactVM = new ContactFragmentVM();
        mBinding.setViewModel(mContactVM);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ListGroupUser listGroupUser = new ListGroupUser();
        ArrayList<ListGroupUser.ListItem> listItems = listGroupUser.convertToListUser();

        RecyclerView recyclerView = mBinding.recyclerContact;
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        RecyclerContactAdapter adapter = new RecyclerContactAdapter();
        adapter.updateItems(listItems);
        recyclerView.setAdapter(adapter);

        adapter.setItemClickListener(mItemClickListener);
        mContactVM.setListener(mSearchCreateClickListener);

    }

    public void setSearchOrCreateClickListener(MainContactListener.SearchCreateClickListener listener) {
        mSearchCreateClickListener = listener;
    }

    public void setItemClickListener(MainContactListener.ItemUserAdapterClickListener listener) {
        mItemClickListener = listener;
    }
}
