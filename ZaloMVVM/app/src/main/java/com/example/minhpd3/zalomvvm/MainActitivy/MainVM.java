package com.example.minhpd3.zalomvvm.MainActitivy;


/**
 * Created by MinhPD3 on 2/7/2017.
 */

public class MainVM {
    public final MainModel model = new MainModel();

    public int getIcon(int position) {
        if (position == 0) {
            return model.getIconMessage();
        } else if (position == 1) {
            return model.getIconContact();
        } else if (position == 2) {
            return model.getIconTimeLine();
        }
        return model.getIconMessage();
    }

    public void setIsFragmentGone(boolean isGone) {
        model.setFragmentGone(isGone);

    }

    public void setToolbar(boolean isShow, boolean isBlue) {
        model.setShowToolbar(isShow);
        model.setBlue(isBlue);
    }
}
