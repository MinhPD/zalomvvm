package com.example.minhpd3.zalomvvm.MessageFragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.minhpd3.zalomvvm.MainContactListener;
import com.example.minhpd3.zalomvvm.UserModel;
import com.example.minhpd3.zalomvvm.databinding.FragmentMessageLayoutBinding;

import java.util.ArrayList;

/**
 * Created by MinhPD3 on 2/7/2017.
 */

public class MessageFragment extends Fragment {

    private FragmentMessageLayoutBinding mBinding;
    private MessageFragmentVM mFragmentVM;
    private MainContactListener.SearchCreateClickListener mListener;
    private MainContactListener.ItemUserAdapterClickListener mListenerItemClick;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentVM = new MessageFragmentVM();
        mBinding = FragmentMessageLayoutBinding.inflate(inflater, container, false);
        mBinding.setViewModel(mFragmentVM);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = mBinding.recyclerMessage;
        RecyclerMessageAdapter messageAdapter = new RecyclerMessageAdapter();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration decoration = new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(decoration);

        ArrayList<UserModel> list = UserModel.getFakeData();
        messageAdapter.updateItems(list);
        recyclerView.setAdapter(messageAdapter);

        mFragmentVM.setListener(mListener);
        messageAdapter.setListener(mListenerItemClick);
    }

    public void setListenerSearchCreate(MainContactListener.SearchCreateClickListener listener) {
        mListener = listener;
    }

    public void setListenerItemclick(MainContactListener.ItemUserAdapterClickListener listener) {
        mListenerItemClick = listener;
    }
}
