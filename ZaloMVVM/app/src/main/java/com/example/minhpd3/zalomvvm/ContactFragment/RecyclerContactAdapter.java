package com.example.minhpd3.zalomvvm.ContactFragment;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.minhpd3.zalomvvm.BaseAdapter;
import com.example.minhpd3.zalomvvm.MainContactListener;
import com.example.minhpd3.zalomvvm.R;
import com.example.minhpd3.zalomvvm.databinding.RecyclerContactTitleItemBinding;
import com.example.minhpd3.zalomvvm.databinding.RecyclerContactUserItemBinding;

/**
 * Created by MinhPD3 on 2/10/2017.
 */

public class RecyclerContactAdapter extends BaseAdapter<ListGroupUser.ListItem, RecyclerView.ViewHolder> {
    private MainContactListener.ItemUserAdapterClickListener mItemUserClickListener;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case ListGroupUser.ListItem.TYPE_KEY:
                RecyclerContactTitleItemBinding titleItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_contact_title_item, parent, false);
                return new ViewHolderTitle(titleItemBinding);

            case ListGroupUser.ListItem.TYPE_USER:
                RecyclerContactUserItemBinding userItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_contact_user_item, parent, false);
                return new ViewHolderUser(userItemBinding);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case ListGroupUser.ListItem.TYPE_KEY:
                ListGroupUser.KeyItem keyItem = (ListGroupUser.KeyItem) mItems.get(position);
                ((ViewHolderTitle) holder).bind(keyItem);
                break;
            case ListGroupUser.ListItem.TYPE_USER:
                ListGroupUser.UserItem userItem = (ListGroupUser.UserItem) mItems.get(position);
                ((ViewHolderUser) holder).bind(userItem);
                break;
        }
    }

    private class ViewHolderTitle extends RecyclerView.ViewHolder {
        private RecyclerContactTitleItemBinding binding;
        private RecyclerContactAdapterTitleVM titleVM;

        public ViewHolderTitle(RecyclerContactTitleItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            titleVM = new RecyclerContactAdapterTitleVM();

        }

        private void bind(ListGroupUser.KeyItem item) {
            binding.setViewModel(titleVM);
            titleVM.setTitle(item.getName());
        }
    }

    private class ViewHolderUser extends RecyclerView.ViewHolder {
        private RecyclerContactUserItemBinding binding;
        private RecyclerContactAdapterUserVM userVM;

        public ViewHolderUser(RecyclerContactUserItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            userVM = new RecyclerContactAdapterUserVM();
        }

        private void bind(ListGroupUser.UserItem item) {
            binding.setViewModel(userVM);
            userVM.setIcon(item.getModel().getIcon());
            userVM.setName(item.getModel().getName());
            userVM.setId(item.getModel().getId());
            userVM.setListener(mItemUserClickListener);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mItems.get(position).getType();
    }

    public void setItemClickListener(MainContactListener.ItemUserAdapterClickListener itemClickListener) {
        mItemUserClickListener = itemClickListener;
    }
}
