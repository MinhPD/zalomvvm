package com.example.minhpd3.zalomvvm.SearchFragment;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.minhpd3.zalomvvm.BaseAdapter;
import com.example.minhpd3.zalomvvm.R;
import com.example.minhpd3.zalomvvm.UserModel;
import com.example.minhpd3.zalomvvm.databinding.RecyclerSearchItemBinding;

/**
 * Created by MinhPD3 on 2/9/2017.
 */

public class RecyclerSearchAdapter extends BaseAdapter<UserModel, RecyclerSearchAdapter.ViewHolder> {

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerSearchItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_search_item, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        UserModel model = mItems.get(position);
        holder.bind(model);
        Log.e("onBindViewHolder", "onBindViewHolder");
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private RecyclerSearchItemBinding binding;
        private RecyclerSearchVM searchVM;

        public ViewHolder(RecyclerSearchItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            searchVM = new RecyclerSearchVM();
        }

        public void bind(UserModel model) {
            binding.setViewModel(searchVM);
            searchVM.setIcon(model.getIcon());
            searchVM.setId(model.getId());
            searchVM.setName(model.getName());
        }
    }
}
