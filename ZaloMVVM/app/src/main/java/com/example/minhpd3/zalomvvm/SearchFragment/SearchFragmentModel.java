package com.example.minhpd3.zalomvvm.SearchFragment;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.example.minhpd3.zalomvvm.R;

/**
 * Created by MinhPD3 on 2/9/2017.
 */

public class SearchFragmentModel extends BaseObservable {
    private int searchNearest = R.string.search_nearest;
    private int remove = R.string.remove;

    @Bindable
    public int getSearchNearest() {
        return searchNearest;
    }

    @Bindable
    public int getRemove() {
        return remove;
    }
}
