package com.example.minhpd3.zalomvvm.SearchFragment;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.example.minhpd3.zalomvvm.BR;

/**
 * Created by MinhPD3 on 2/9/2017.
 */

public class RecyclerSearchModel extends BaseObservable {
    private int id;
    private int icon;
    private String name;

    public void setId(int id) {
        this.id = id;
        notifyPropertyChanged(BR.id);
    }

    public void setIcon(int icon) {
        this.icon = icon;
        notifyPropertyChanged(BR.icon);
    }

    public void setName(String name) {
        this.name = name;
        notifyPropertyChanged(BR.name);
    }

    @Bindable
    public int getId() {
        return id;
    }

    @Bindable
    public int getIcon() {
        return icon;
    }

    @Bindable
    public String getName() {
        return name;
    }
}
