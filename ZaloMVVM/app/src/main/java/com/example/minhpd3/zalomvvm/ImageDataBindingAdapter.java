package com.example.minhpd3.zalomvvm;

import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by MinhPD3 on 2/8/2017.
 */

public class ImageDataBindingAdapter {

    @BindingAdapter("android:src")
    public static void setImageResource(ImageView imageView, int resource) {
        imageView.setImageResource(resource);
    }
    @BindingAdapter("android:visibility")
    public static void setVisibility(View view,boolean isShow){
        if(isShow)
        {
            view.setVisibility(View.VISIBLE);
        }
        else {
            view.setVisibility(View.GONE);
        }
    }
}
