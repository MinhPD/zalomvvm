package com.example.minhpd3.zalomvvm.MainActitivy;

import android.databinding.DataBindingUtil;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.minhpd3.zalomvvm.ContactFragment.ContactFragment;
import com.example.minhpd3.zalomvvm.MainContactListener;
import com.example.minhpd3.zalomvvm.MessageFragment.MessageFragment;
import com.example.minhpd3.zalomvvm.R;
import com.example.minhpd3.zalomvvm.SearchFragment.SearchFragment;
import com.example.minhpd3.zalomvvm.TimeLineFragment.TimeLineFragment;
import com.example.minhpd3.zalomvvm.ViewPager.ViewPagerAdapter;
import com.example.minhpd3.zalomvvm.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements MainContactListener.SearchCreateClickListener,
        MainContactListener.ItemUserAdapterClickListener, View.OnClickListener {
    private MainVM mMainVM;
    private ActivityMainBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mMainVM = new MainVM();
        mBinding.setViewModel(mMainVM);

        setSupportActionBar(mBinding.searchToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        // set up view pager
        viewPagerAdapter.addFragment(new MessageFragment());
        viewPagerAdapter.addFragment(new ContactFragment());
        viewPagerAdapter.addFragment(new TimeLineFragment());
        viewPagerAdapter.addFragment(new SearchFragment());

        mBinding.pagerMain.setAdapter(viewPagerAdapter);

        // set up tablayout
        mBinding.tablayout.setupWithViewPager(mBinding.pagerMain);
        mBinding.tablayout.getTabAt(0).setIcon(mMainVM.getIcon(0));
        mBinding.tablayout.getTabAt(1).setIcon(mMainVM.getIcon(1));
        mBinding.tablayout.getTabAt(2).setIcon(mMainVM.getIcon(2));
        mBinding.tablayout.getTabAt(3).setIcon(mMainVM.getIcon(3));

        //set listener for click event on message fragment
        if (viewPagerAdapter.getItem(0) != null) {
            MessageFragment messageFragment = (MessageFragment) viewPagerAdapter.getItem(0);
            messageFragment.setListenerSearchCreate(this);
            messageFragment.setListenerItemclick(this);
        }

        if (viewPagerAdapter.getItem(1) != null) {
            ContactFragment contactFragment = (ContactFragment) viewPagerAdapter.getItem(1);
            contactFragment.setItemClickListener(this);
            contactFragment.setSearchOrCreateClickListener(this);
        }

        //set listener for back arrow on toolbar
        mBinding.searchToolbar.setNavigationOnClickListener(this);

    }

    @Override
    public void itemClick(int id) {
        mMainVM.setIsFragmentGone(true);
        mMainVM.setToolbar(true, true);
        openSearchFragment();
    }

    @Override
    public void searchClick() {
        Toast.makeText(this, "Search Clicked", Toast.LENGTH_SHORT).show();
        mMainVM.setIsFragmentGone(true);
        mMainVM.setToolbar(true, true);
        openSearchFragment();
    }

    private void openSearchFragment() {
        SearchFragment searchFragment = new SearchFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(mBinding.frameMain.getId(), searchFragment, "SEARCHFRAGMENT");
        transaction.addToBackStack("SEARCHFRAGMENT");
        transaction.commit();
    }

    private void removeSearchFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        SearchFragment searchFragment = (SearchFragment) getSupportFragmentManager().findFragmentByTag("SEARCHFRAGMENT");
        transaction.remove(searchFragment);
        transaction.commit();
    }

    @Override
    public void createGroupClick() {
        Toast.makeText(this, "Create Group Clicked", Toast.LENGTH_SHORT).show();
        mMainVM.setIsFragmentGone(true);
        mMainVM.setToolbar(true, false);
        openSearchFragment();
    }

    @Override
    public void onClick(View v) {
        removeSearchFragment();
        mMainVM.setIsFragmentGone(false);
        mMainVM.setToolbar(false, false);
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mMainVM.setIsFragmentGone(false);
        mMainVM.setToolbar(false, false);
    }
}
