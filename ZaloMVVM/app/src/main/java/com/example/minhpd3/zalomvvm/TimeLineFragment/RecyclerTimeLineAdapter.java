package com.example.minhpd3.zalomvvm.TimeLineFragment;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.example.minhpd3.zalomvvm.BaseAdapter;
import com.example.minhpd3.zalomvvm.UserModel;

/**
 * Created by MinhPD3 on 2/10/2017.
 */

public class RecyclerTimeLineAdapter extends BaseAdapter<UserModel, RecyclerTimeLineAdapter.ViewHolder> {
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
