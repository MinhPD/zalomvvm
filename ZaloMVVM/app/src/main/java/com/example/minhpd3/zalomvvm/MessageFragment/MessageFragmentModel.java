package com.example.minhpd3.zalomvvm.MessageFragment;


import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.example.minhpd3.zalomvvm.R;

import java.util.ArrayList;

/**
 * Created by MinhPD3 on 2/8/2017.
 */

public class MessageFragmentModel extends BaseObservable {

    private int iconSearch = R.drawable.icon_search;
    private int iconCreateGroup = R.drawable.icon_create_group;

    @Bindable
    public int getIconSearch() {
        return iconSearch;
    }

    @Bindable
    public int getIconCreateGroup() {
        return iconCreateGroup;
    }

}
