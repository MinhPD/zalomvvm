package com.example.minhpd3.zalomvvm;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MinhPD3 on 2/7/2017.
 */

public abstract class BaseAdapter<T, D extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<D> {
    protected List<T> mItems;

    public BaseAdapter() {
        mItems = new ArrayList<>();
    }

    public void updateItems(List<T> items) {
        if (items != null) {
            mItems = items;
            notifyDataSetChanged();
        }
    }

    public T getItemByPosition(int position) {
        if (position < getItemCount()) {
            return mItems.get(position);
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

}
