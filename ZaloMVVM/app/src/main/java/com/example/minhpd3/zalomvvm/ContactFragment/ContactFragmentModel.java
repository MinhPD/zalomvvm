package com.example.minhpd3.zalomvvm.ContactFragment;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.example.minhpd3.zalomvvm.R;

/**
 * Created by MinhPD3 on 2/9/2017.
 */

public class ContactFragmentModel extends BaseObservable {
    private int iconSearch = R.drawable.icon_search;
    private int iconCreateGroup = R.drawable.icon_create_group;

    @Bindable
    public int getIconSearch() {
        return iconSearch;
    }

    @Bindable
    public int getIconCreateGroup() {
        return iconCreateGroup;
    }
}
