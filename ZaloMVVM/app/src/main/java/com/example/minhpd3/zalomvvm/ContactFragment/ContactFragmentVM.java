package com.example.minhpd3.zalomvvm.ContactFragment;

import com.example.minhpd3.zalomvvm.MainContactListener;

/**
 * Created by MinhPD3 on 2/9/2017.
 */

public class ContactFragmentVM {
    public ContactFragmentModel model = new ContactFragmentModel();
    private MainContactListener.SearchCreateClickListener mListener;

    public void onSearchClick() {
        mListener.searchClick();
    }

    public void onCreateContactClick() {
        mListener.createGroupClick();
    }

    public void setListener(MainContactListener.SearchCreateClickListener listener) {
        mListener = listener;
    }
}
