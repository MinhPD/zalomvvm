package com.example.minhpd3.zalomvvm.TimeLineFragment;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.example.minhpd3.zalomvvm.R;

/**
 * Created by MinhPD3 on 2/10/2017.
 */

public class RecyclerTimeLineAdapterModel extends BaseObservable {
    private int id;
    private int icon;
    private String name;
    private int image;
    private boolean isShowImage;
    private int textLike = R.string.textLike;
    private int textComment = R.string.textComment;

    @Bindable
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Bindable
    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    @Bindable
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Bindable
    public int getImage() {
        return image;
    }

    @Bindable
    public void setImage(int image) {
        this.image = image;
    }

    @Bindable
    public boolean isShowImage() {
        return isShowImage;
    }

    @Bindable
    public int getTextComment() {
        return textComment;
    }

    @Bindable
    public int getTextLike() {
        return textLike;
    }

    public void setShowImage(boolean showImage) {
        isShowImage = showImage;
    }
}

