package com.example.minhpd3.zalomvvm.SearchFragment;

/**
 * Created by MinhPD3 on 2/9/2017.
 */

public class RecyclerSearchVM {
    public RecyclerSearchModel model = new RecyclerSearchModel();

    public void onItemClick(int id) {

    }

    public void setId(int id) {
        model.setId(id);
    }

    public void setName(String name) {
        model.setName(name);
    }

    public void setIcon(int icon) {
        model.setIcon(icon);
    }
}
