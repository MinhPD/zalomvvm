package com.example.minhpd3.zalomvvm.MessageFragment;

import com.example.minhpd3.zalomvvm.MainContactListener;

/**
 * Created by MinhPD3 on 2/8/2017.
 */

public class RecyclerMessageVM {
    private MainContactListener.ItemUserAdapterClickListener mListener;
    public RecyclerMessageModel model = new RecyclerMessageModel();

    public void setNameUser(String name) {
        model.setName(name);
    }

    public void setIconUser(int icon) {
        model.setIcon(icon);
    }

    public void setIdUser(int id) {
        model.setId(id);
    }

    public void onItemClick(int id) {
        mListener.itemClick(id);
    }

    public void setListener(MainContactListener.ItemUserAdapterClickListener listener) {
        mListener = listener;
    }
}
