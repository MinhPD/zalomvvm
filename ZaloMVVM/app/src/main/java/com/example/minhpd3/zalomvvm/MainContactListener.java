package com.example.minhpd3.zalomvvm;

/**
 * Created by MinhPD3 on 2/9/2017.
 */

public interface MainContactListener {
    interface ItemUserAdapterClickListener {
        void itemClick(int id);
    }

    interface SearchCreateClickListener {
        void searchClick();

        void createGroupClick();
    }
}
