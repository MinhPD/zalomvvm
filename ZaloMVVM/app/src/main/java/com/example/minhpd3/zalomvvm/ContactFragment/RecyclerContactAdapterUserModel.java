package com.example.minhpd3.zalomvvm.ContactFragment;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.example.minhpd3.zalomvvm.BR;

/**
 * Created by MinhPD3 on 2/10/2017.
 */

public class RecyclerContactAdapterUserModel extends BaseObservable {
    private int id;

    @Bindable
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
        notifyPropertyChanged(BR.id);
    }

    private int icon;
    private String name;

    @Bindable
    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    @Bindable
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
