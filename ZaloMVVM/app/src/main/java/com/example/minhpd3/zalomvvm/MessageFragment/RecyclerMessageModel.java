package com.example.minhpd3.zalomvvm.MessageFragment;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.example.minhpd3.zalomvvm.BR;

/**
 * Created by MinhPD3 on 2/8/2017.
 */

public class RecyclerMessageModel extends BaseObservable {
    private int icon;
    private String name;
    private int id;

    public void setId(int id) {
        this.id = id;
    }

    @Bindable
    public int getId() {
        return id;
    }

    @Bindable
    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
        notifyPropertyChanged(BR.icon);
    }

    @Bindable
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        notifyPropertyChanged(BR.name);
    }

    public RecyclerMessageModel() {
    }

}
