package com.example.minhpd3.zalomvvm.MainActitivy;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.example.minhpd3.zalomvvm.BR;
import com.example.minhpd3.zalomvvm.R;


/**
 * Created by MinhPD3 on 2/8/2017.
 */

public class MainModel extends BaseObservable {
    private int iconMessage = R.drawable.icon_message;
    private int iconContact = R.drawable.icon_contact;
    private int iconTimeLine = R.drawable.icon_timeline;

    @Bindable
    public int getIconContact() {
        return iconContact;
    }

    @Bindable
    public int getIconTimeLine() {
        return iconTimeLine;
    }

    private boolean isFragmentGone;
    private boolean isBlue = true;
    private boolean isShowToolbar = false;

    @Bindable
    public boolean isBlue() {
        return isBlue;
    }

    public void setBlue(boolean blue) {
        isBlue = blue;
        notifyPropertyChanged(com.example.minhpd3.zalomvvm.BR.blue);
    }

    public void setShowToolbar(boolean showToolbar) {
        isShowToolbar = showToolbar;
        notifyPropertyChanged(BR.showToolbar);
    }

    @Bindable
    public boolean isShowToolbar() {
        return isShowToolbar;

    }

    public void setFragmentGone(boolean fragmentGone) {
        isFragmentGone = fragmentGone;
        notifyPropertyChanged(com.example.minhpd3.zalomvvm.BR.fragmentGone);
    }

    @Bindable
    public boolean isFragmentGone() {

        return isFragmentGone;
    }

    @Bindable
    public int getIconMessage() {
        return iconMessage;
    }


}
