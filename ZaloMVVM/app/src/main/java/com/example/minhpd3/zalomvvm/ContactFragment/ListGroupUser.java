package com.example.minhpd3.zalomvvm.ContactFragment;

import com.example.minhpd3.zalomvvm.UserModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * Created by MinhPD3 on 2/10/2017.
 */

public class ListGroupUser {
    private ArrayList<UserModel> listUser;
    private HashMap<String, List<UserModel>> groupUser;

    //create one by one group with key and list user in each group
    public void createGroupUser() {
        groupUser = new HashMap<>();
        listUser = UserModel.getFakeData();
        for (UserModel model : listUser) {
            String keyHashmap = String.valueOf(model.getName().charAt(0));
            if (groupUser.containsKey(keyHashmap)) {
                groupUser.get(keyHashmap).add(model);
            } else {
                List<UserModel> listUserHashMap = new ArrayList<>();
                listUserHashMap.add(model);
                groupUser.put(keyHashmap, listUserHashMap);
            }
        }
    }

    // convert all group to one list
    public ArrayList<ListItem> convertToListUser() {
        createGroupUser();

        ArrayList<ListItem> listItems = new ArrayList<>();
        for (String key : groupUser.keySet()) {
            KeyItem keyItem = new KeyItem();
            keyItem.setKey(key);
            listItems.add(keyItem);
            for (UserModel model : groupUser.get(key)) {
                UserItem userItem = new UserItem();
                userItem.setModel(model);
                listItems.add(userItem);
            }
        }
        Collections.sort(listItems, new UserNameComparator());
        return listItems;
    }

    public class UserNameComparator implements Comparator<ListItem> {


        @Override
        public int compare(ListItem o1, ListItem o2) {
            return o1.getName().compareToIgnoreCase(o2.getName());
        }
    }

    public class KeyItem extends ListItem {
        private String key;


        public void setKey(String key) {
            this.key = key;
        }

        @Override
        public int getType() {
            return TYPE_KEY;
        }

        @Override
        public String getName() {
            return key;
        }
    }

    public class UserItem extends ListItem {
        private UserModel model;

        public UserModel getModel() {
            return model;
        }

        public void setModel(UserModel model) {
            this.model = model;
        }

        @Override
        public int getType() {
            return TYPE_USER;
        }

        @Override
        public String getName() {
            return model.getName();
        }
    }

    public abstract class ListItem {

        public static final int TYPE_KEY = 0;
        public static final int TYPE_USER = 1;

        abstract public int getType();

        abstract public String getName();
    }
}
